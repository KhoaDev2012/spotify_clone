"use client";

import { HiHome } from "react-icons/hi";
import { BsSearch } from "react-icons/bs";
import { usePathname } from "next/navigation";

import Box from "@/components/Box";
import Library from "@/components/Library";
import SidebarItem from "@/components/SidebarItem";

interface SidebarProps {
  children: React.ReactNode;
}

const Sidebar = ({ children }: SidebarProps) => {
  const pathname = usePathname();

  const routes = [
    {
      icon: HiHome,
      label: "Home",
      active: pathname !== "/search",
      href: "/",
    },
    {
      icon: BsSearch,
      label: "Search",
      active: pathname === "/search",
      href: "/search",
    },
  ];
  return (
    <div className="flex h-full">
      <div className="hidden md:flex flex-col gap-y-2 h-full w-[300px] bg-black p-2">
        <Box>
          <div className="flex flex-col gap-y-4 px-6 py-4">
            {routes.map((route) => (
              <SidebarItem key={route.label} {...route} />
            ))}
          </div>
        </Box>
        <Box className="bg-green-900 overflow-y-auto h-full">
          <Library />
        </Box>
      </div>

      <main className="h-full flex-1 overflow-y-auto py-2">{children}</main>
    </div>
  );
};

export default Sidebar;

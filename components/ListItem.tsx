"use client";

import Image from "next/image";
import { useRouter } from "next/navigation";
import { FaPlay } from "react-icons/fa";

interface ListItemProps {
  name: string;
  href: string;
  image: string;
}

const ListItem = ({ name, href, image }: ListItemProps) => {
  const router = useRouter();

  const handleClick = () => {
    // Add authentication before add liked song
    router.push(href);
  };

  return (
    <button
      onClick={handleClick}
      className="relative group flex items-center rounded-md overflow-hidden gap-x-4 transition pr-4 bg-neutral-100/10 hover:bg-neutral-100/20"
    >
      <div className="relative min-h-[64px] min-w-[64px]">
        <Image fill src={image} className="object-cover" alt="images" />
      </div>
      <p className="font-medium truncate py-5">{name}</p>

      <div className="absolute opacity-0 transition rounded-full flex items-center justify-center bg-green-500 p-3 drop-shadow-md right-5 group-hover:opacity-100 hover:scale-110">
        <FaPlay size={22} />
      </div>
    </button>
  );
};

export default ListItem;

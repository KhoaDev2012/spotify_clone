"use client";

import React from "react";
import { twMerge } from "tailwind-merge";
import { useRouter } from "next/navigation";
import { RxCaretLeft, RxCaretRight } from "react-icons/rx";
import { HiHome } from "react-icons/hi";
import { BiSearch } from "react-icons/bi";
import Button from "@/components/Button";

interface HeaderProps {
  children: React.ReactNode;
  className?: string;
}

const Header = ({ children, className }: HeaderProps) => {
  const router = useRouter();

  const handleLogout = () => {
    // TODO: logout in future
  };

  return (
    <div
      className={twMerge(
        `
        h-fit
        bg-gradient-to-b
        from-emerald-800
        p-6
        `,
        className
      )}
    >
      <div className="w-full mb-4 flex items-center justify-between">
        <div className="hidden md:flex gap-x-2 items-center">
          <button
            onClick={() => router.back()}
            className="rounded-full bg-black flex items-center justify-center hover:opacity-75 transition"
          >
            <RxCaretLeft size={36} />
          </button>
          <button
            onClick={() => router.forward()}
            className="rounded-full bg-black flex items-center justify-center hover:opacity-75 transition"
          >
            <RxCaretRight size={36} />
          </button>
        </div>

        {/* MOBILE UI */}
        <div className="flex md:hidden gap-x-2 items-center justify-center ">
          <button className="rounded-full bg-white p-2 flex items-center justify-center transition hover:opacity-75">
            <HiHome size={20} className="text-black" />
          </button>
          <button className="rounded-full bg-white p-2 flex items-center justify-center transition hover:opacity-75">
            <BiSearch size={20} className="text-black" />
          </button>
        </div>

        {/* Authentication Section */}
        <div className="flex items-center justify-between gap-x-4">
          <>
            <div>
              <Button
                className="bg-transparent text-white font-medium"
                onClick={() => {}}
              >
                Sign up
              </Button>
            </div>
            <div>
              <Button
                className="bg-white px-6 py-2 text-green-600"
                onClick={() => {}}
              >
                Sign in
              </Button>
            </div>
          </>
        </div>
      </div>
      {children}
    </div>
  );
};

export default Header;

import Header from "@/components/Header";
import ListItem from "@/components/ListItem";

export default function Home() {
  return (
    <div className="w-full h-full bg-neutral-900 rounded-lg overflow-hidden overflow-y-auto">
      <Header>
        <div className="mb-2">
          <h1 className="text-3xl text-white font-semibold">Welcome back</h1>

          {/* LIST ITEM MUSIC */}
          <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 2xl:grid-cols-4 gap-3 mt-4">
            <ListItem
              image="/images/liked.jpg"
              name="Liked Songs"
              href="liked"
            />
          </div>
        </div>
      </Header>

      {/* CONTENT */}
      <div className="mt-2 mb-6 px-6">
        <div className="flex justify-between items-center">
          <h1 className="text-white font-semibold text-2xl">Newest Songs</h1>
        </div>

        {/* Render list song in future */}
        <div>List of song</div>
      </div>
    </div>
  );
}
